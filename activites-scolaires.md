---
layout: page
title: Activités pour les scolaires 2018
year: '2018'
---

Voici les activités qui seront présentées aux scolaires le vendredi 12
octobre 2018 de 9h00 à 18h00. Les inscriptions des classes se font auprès
du
[service communication de l'université](http://www.sciences.u-psud.fr/fr/actualites/fete-de-la-science-edition-2017/visites-1.html)
à partir de la mi-juin. Nous sommes complet pour 2018.

Télécharger le [planning](../documents/planning-scolaires-2018.pdf)
ou la
[plaquette]({{site.baseurl}}/documents/plaquette-scolaires-2018.pdf).

<table>
  <tr>
  {% for activite in site.activites %}
    {% if activite[page.year].scolaires %}
      {% include activite-in-table.html activite=activite year=page.year %}
    {% endif %}
  {% endfor %}
  </tr>
</table>
