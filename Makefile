build:
	bundle exec jekyll build

serve:
	bundle exec jekyll serve -w -b ''

DOCUMENTS_SVG=$(wildcard planning*.svg)
DOCUMENTS_ODP=$(wildcard plaquette*.odp)
DOCUMENTS_PDF=$(DOCUMENTS_ODP:%.odp=documents/%.pdf) $(DOCUMENTS_SVG:%.svg=documents/%.pdf)
ACTIVITES=$(wildcard _activites/*.md)
ACTIVITES_TEX=$(ACTIVITES:_activites/%.md=affiches/%.tex)

affiches: $(ACTIVITES_TEX)

install: build $(DOCUMENTS_PDF)
	rsync -av --delete _site/ pl-ssh.lri.fr:/users/projets/fds/

documents/planning-scolaires-%.pdf: planning-scolaires-%.svg
	inkscape $< --export-pdf=$@

documents/plaquette-%.pdf: plaquette-%.odp
	libreoffice --convert-to pdf $< --outdir documents

PARTICIPANTS=`cat _site/attestations/index.mk`

.PHONY: attestations

attestations:
	for participant in $(PARTICIPANTS); do \
	    echo "---\nlayout: attestation\nid: $$participant\n---\n" > attestations/$$participant.tex; \
	done
	jekyll build
	-ln -s ~/Paperasse/signature.pdf _site/attestations
	for participant in $(PARTICIPANTS); do \
	    cd _site/attestations; \
	    xelatex $$participant.tex; \
	done
	rm _site/attestations/signature.pdf

affiches/%.tex: _activites/%.md
	pandoc $< -o $@
