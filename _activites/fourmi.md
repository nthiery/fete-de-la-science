---
layout: activite
title: Aide-moi à sortir !
level: primaire, collège
'2016':
  author:
  - falque
  scolaires:
    salle: Bibliothèque - Bâtiment 650 Ada Lovelace
'2017':
  author:
  - falque
  scolaires:
    salle: bibliothèque - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Bâtiment 450
    participants: ~5 * 20
'2018':
  author:
  - falque
  - pavao
  - bartenlian
  scolaires:
    salle: bibliothèque - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: Bâtiment 450
bgcolor: LightYellow
keywords: 
logo: fourmi-logo.png
images: fourmi_1.jpg
myid: fourmi
---

La fourmi est enfermée dans un labyrinthe aux mille embûches. Elle est
toute perdue, mais courageuse et très obéissante. 

Ensemble nous la guiderons pas à pas jusqu'à la sortie. Ce sera
l'occasion d'écrire nos premiers programmes : donner une suite d'ordres
simples, les répéter, les adapter à l'environnement.
