/*
  Une alarme oscillante
  Auteur: Malo Pessel
  Buzzer branché sur le pin 8
*/
void setup() {
}

void loop() {
  for(int j=440;j<=2000;j=j+10){
    tone(8, j,10);
    delay (9);
  }
  for(int j=2000;j>=440;j=j-10){
    tone(8, j,10);
    delay (9);
  }
}
