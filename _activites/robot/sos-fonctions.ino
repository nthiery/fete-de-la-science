/*
  SOS: émets le signal de détresse SOS en code morse avec la led
  connectée sur le port 13
  Auteur: Corentin Ravel
 */
int led = 13;

// Émets un flash long
void trait () {
  digitalWrite(led, HIGH);
  delay(500);
  digitalWrite(led, LOW);
  delay(500);
}

// Émets un flash court
void point () {
  digitalWrite(led, HIGH);
  delay(100);
  digitalWrite(led, LOW);
  delay(100);
}

void setup() {
  pinMode(led, OUTPUT);
}

void loop() {
  point ();
  point ();
  point ();
  trait ();
  trait ();
  trait ();
  point ();
  point ();
  point ();

  delay (1000);
}
