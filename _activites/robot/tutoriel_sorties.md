---
layout: page
title: "Tutoriel: «Les sorties: agir sur le monde»"
---

## Prérequis

- Une carte arduino et son câble USB
- L'application arduino installée
- Optionnel: une (grosse) led, un petit moteur, un buzzer

## Instructions

- Lancer l'application Arduino.
- Brancher la carte Arduino sur un port USB.
- Ouvrir l'exemple `Blink` (menu Fichier -> Exemples -> Basic -> Blink).
- Cliquer sur le bouton téléverser.

Après quelques secondes, une led se mettra à clignoter lentement.

## Comment ça marche?

Regardons le programme pour comprendre ce qu'il fait:
{% highlight c++ linenos %}
{% include_relative Blink.ino %}
{% endhighlight %}

Le coeur du programme est composé des quatre instructions après le mot
`loop`:

    digitalWrite(led, HIGH);
    delay(1000);
    digitalWrite(led, LOW);
    delay(1000);

- La première instruction allume la led (`write`: écrire; `high`: haut)
- La deuxième attend 1000 millisecondes, soit une seconde (`delay`: délai/attendre)
- La troisième éteints la led (`low`: bas)
- La quatrième attend à nouveau une seconde.

Ces quatre instructions sont répétées en boucle (`loop`: faire une
boucle, comme les avions qui font des loopings). Ce qui fait clignoter
la lampe.

## Plus de lumière!

Nous venons de voir qu'avec de simples mots (le programme), on peut
agir sur le monde physique! Mais ce n'est peut-être pas encore très
convainquant?

Nous allons exploiter une faculté majeure des ordinateurs arduino: la
facilité d'y brancher des composants électroniques, grâce aux
multiples prises.

- Débrancher l'arduino;
- Prendre une (grosse) led;
- Repérer les prises nommées GND (ground: terre) et 13;
- Insérer les deux pattes de la led chacune dans une de ces prises.
- Rebrancher l'arduino.

La led devrait clignoter avec une lumière maintenant bien visible:

TODO: insérer une photo

Notes:

- Vous avez en fait une chance sur deux: la led est en effet un
  composant asymétrique; s'il ne se passe rien, inversez les deux
  pattes. Vous noterez au passage que l'une des deux pattes est plus
  courte; c'est elle qui doit aller sur la terre.

- En principe, il faut brancher la led en série avec une résistance
  pour les protéger. Les grosses led qu'on utilise dans l'atelier
  tiennent le choc. Si vous n'en avez pas, vous pouvez essayer avec
  une led normale; au pire vous la grillerez; cela ne coûte pas une
  fortune.

## Du mouvement!

Ce que l'on veut vraiment fabriquer, c'est un robot. Il faut que cela
bouge!

Instructions:
- Prendre un petit moteur
- Le brancher sur les prises GND et 13

Si tout se passe bien, le moteur s'allumera et s'éteindra.

Note:

- Un moteur cela consomme pas mal d'électricité. L'arduino n'est pas
  conçu pour alimenter directement des composants nécessitant de la
  puissance. Il est possible que le moteur ne démarre pas, et dans
  tous les cas il vaut mieux ne pas prolonger l'expérience pour ne pas
  surchauffer l'arduino. Par la suite, on utilisera une carte
  d'extension pour commander des moteurs.

- Les moteurs pas-à-pas et les servo-moteurs nécessitent de
  l'électronique plus complexe pour les commander. Là encore, on peut
  utiliser une carte d'extension.

Pour aller plus loin, voir le
[tutoriel sur les moteurs](../tutoriel_moteurs).

## Les *effecteurs*

La fonction `digitalWrite` permet de régler la tension entre la terre
(0V) et un pin comme `13`. C'est la seule manière qu'ait arduino
d'agir sur son environnement. Mais c'est suffisant! Lorsque l'on veut
agir d'une autre manière (émettre une lumière, faire tourner un
moteur, ...) on utilisera un *effecteur* pour convertir cette tension
en action physique.

## Pour aller plus loin: détails sur le programme

### Fonctions

La syntaxe

    void unNom() {
        ...
    }

Permet de définir une nouvelle instruction en regroupant plusieurs
instructions existantes. On appelle cela une *fonction*. Ici, la
fonction s'appelle unNom. Pour l'utiliser, il suffit d'insérer dans le
programme:

    unNom();

Dans notre programme, il y a deux fonctions qui ont un rôle spécial
pour arduino:

- la fonction `setup` qui est exécutée une fois au début; on y met
  donc les instructions d'initialisation.

- la fonction `loop` qui est exécutée en boucle par la suite

## Défis

### Un clignotant

Adapter le code pour que:

- La led clignote plus lentement
- La led clignote plus vite
- La led clignote beaucoup plus vite

  On remarquera que, en dessous d'un certain seuil, la led semble ne
  plus clignoter. Que se passe-t'il?

  Faire clignoter la led le plus rapidement possible en gardant le
  clignotement visible. Déterminer le nombre de clignotement par
  secondes. Mettre cela en rapport avec le nombre d'images par
  secondes dans un film.

### Un phare

Faire clignoter la led au rythme de votre phare préféré.

### SOS

Émettre le signal de détresse SOS en code morse: `... - - - ...`, soit
trois clignotements courts, trois clignotements longs, trois
clignotements courts. Répéter le signal en insérant une pause de une
seconde entre chaque émission.

Le programme obtenu ressemble probablement à [cela](sos.ino). Cela
fait beaucoup de répétition! Si l'on veut par exemple changer la durée
des traits, il faut changer beaucoup d'endroit. On va améliorer cela
en définissant des fonctions.

Insérer la définition de la fonction suivante juste au dessus de `setup`:

    void trait() {
        digitalWrite(led, HIGH);
        delay(1000);
        digitalWrite(led, LOW);
        delay(1000);
    }

Ajouter de même une fonction point(). Modifier ensuite le programme
principal dans `loop` pour qu'il appelle les fonctions `point` et
`trait`.

[Solution](../sos-fonctions.ino)

### Code morse

Étendre le programme précédent pour pouvoir émettre un message de
votre choix en code morse.

Indication: pour chaque lettre de l'alphabet, écrire une fonction qui
émette. Par exemple, pour la lettre `s`, on écrira la fonction:

    void() morse_s() {
        point();
        point();
        point();
    }

### Sonner l'alarme!

On pourra utiliser un "buzzer", et la fonction
[tone](https://www.arduino.cc/reference/en/language/functions/advanced-io/tone/).
Par exemple, avec le buzzer branché entre `GND` et `8`, l'instruction
suivante émettra un son de fréquence 440Hz (la note «La») pendant une
seconde (1000ms):

    tone(8, 440, 1000);

Pour les branchements, voir le
[tutoriel](https://www.arduino.cc/en/Tutorial/ToneMelody). Voir aussi
`Exemple -> Digital -> toneMelody` pour un exemple plus avancé.

Changer le son du buzzer pour qu'il ressemble plus à celui d'une
alarme (uuuiiiuuuiiiuuuiii). On pourra:

- alterner entre deux sons ([solution](../alarme_biton.ino));
- utiliser une boucle `for` pour augmenter progressivement la fréquence ([solution](../alarme_montante.ino));
- faire osciller la fréquence ([solution](../alarme_oscillante.ino));
- utiliser un cosinus pour faire osciller la fréquence;
- ...
