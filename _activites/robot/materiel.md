---
layout: page
title: Quel matériel choisir?
---

Pour s'initier à la programmation et la robotique il y a de nombreuses
options; le choix sera principalement un compromis entre simplicité de
mise en oeuvre, aspects que l'on souhaite, ou pas, aborder
(électronique, mécanique, programmation, ...), souplesse d'utilisation
et prix. Nous listons ci-dessous quelques unes des options classiques.

Dans le cas de cet atelier, mon choix s'est porté sur
[Arduino](arduino.cc) en fonction des critères suivants:

- Être abordable par le plus grand nombre au niveau du coût et de la
  difficulté de mise en oeuvre. Par exemple qu'un enfant de fin de
  primaire puisse comprendre les idées principales pendant l'atelier
  puis réaliser le même robot, ou mieux un autre robot, avec l'aide
  d'un adulte, quitte à y consacrer un peu de temps.

- Utiliser du matériel et du logiciel libre

- Mettre en jeu tous les aspects: mécanique, électronique,
  informatique

- Ne (presque) pas nécessiter de soudure.

## Une petite liste de matériels

- [Arduino](arduino.cc): un mini ordinateur conçu pour interagir avec
  des circuits électroniques. La programmation se fait depuis une
  application dédiée simple d'utilisation que l'on installe sur son
  ordinateur.

- [microbit](http://microbit.org): une carte compatible Arduino, avec
  plusieurs capteurs et effecteurs directement intégrés, pour se
  concentrer sur la programmation. La carte microbit a été développée
  pour les écoles en Grande Bretagne par un consortium incluant la
  BBC, google, microsoft, ... et distribuée à des millions d'élèves.

- [RoboBox](https://www.robobox.fr/) propose des kits basés sur
  arduino pour construire des robots. Le concept a l'air de laisser
  une certaine liberté de construction, tout en permettant de se
  concentrer principalement sur la programmation, plutôt que les
  aspects mécaniques ou électroniques.

- [Raspberry Pi](https://www.raspberrypi.org/): même concept que
  l'Arduino, mais avec un ordinateur plus évolué (système
  d'exploitation complet sous GNU/Linux). Prise en main plus complexe,
  mais les possibilités de programmation sont nettement plus riches.

- [Thymio](https://www.thymio.org/fr:thymio): mécanique et
  électronique toute faite. Pratique pour se concentrer sur la partie
  programmation. Bien adapté par exemple pour des séquences
  pédagogiques courtes.

- [Lego Mindstorms](https://fr.wikipedia.org/wiki/Lego_Mindstorms):
  composants électroniques et mécaniques compatibles lego, d'où une
  grande simplicité de mise en oeuvre. Mais nettement plus cher et on
  est plus enfermé dans la gamme d'un fabriquant.

- [VEX Robotics](https://www.vexrobotics.com/) Similaire à Lego
  Mindstorms.
