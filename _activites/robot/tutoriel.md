---
layout: page
title: Tutoriel
---

Ce tutoriel vous guide progressivement pour reproduire chez vous les
différents montages présentés pendant l'atelier, jusqu'au robot
suiveur de ligne, ou mieux vos propres projets! Pour donner un ordre
de grandeur, la construction du robot prend une petite semaine à mes
stagiaires de troisième.

- [Les sorties: agir sur le monde](../tutoriel_sorties/)
- [Les moteurs](../tutoriel_moteurs/)
- [Les entrées: sentir le monde](../tutoriel_entrees/)
- [Interactions](../tutoriel_interaction/) (à venir)
- [Le robot suiveur](../tutoriel_robot_suiveur/) (en cours)
