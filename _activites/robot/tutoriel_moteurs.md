---
layout: page
title: "Tutoriel: contrôler des moteurs"
---

# Prérequis

Comme nous l'avons mentionné, les sorties d'un arduino ne délivrent
pas assez de puissance pour piloter directement des moteurs. Il faut
donc passer par un circuit d'amplification, voire des circuits plus
complexes pour le pilotage de moteurs pas à pas (TODO: liens). Pour se
simplifier la tâche, on peut passer par une carte d'extension toute
faite. Ici, nous utiliserons:


- La [carte d'extension de commande de moteurs d'Adafruit](https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino).

- Un moteur à courant continu

- Ce [moteur pas à pas](https://snootlab.com/adafruit/384-petit-moteur-stepper-avec-reducteur-5v-512-pas.html)

# Installation du pilote

Télécharger le pilote de la carte d'extension
(Adafruit Motor Shield V2 Library) depuis la
[page web d'installation](https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/install-software)

L'extraire dans `Dossier Personnel -> sketchbook -> libraries`. Cela y
créé un dossier `Adafruit_Motor_Shield_V2_Library-master` qu'il faut
renommer en `Adafruit_Motor_Shield_V2_Library` (ou tout autre nom simple).

Référence: [](https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino)

## Contournement de boggue

Avec une version un peu ancienne du logiciel arduino (par exemple
celle fourni par défaut sur Ubuntu), il se peut qu'à la compilation il
y ait un message d'erreur «yield n'est pas déclaré dans ce contexte».
Dans ce cas, modifier le fichier `Adafruit_MotorShield.cpp` dans le
dossier ci-dessus, pour supprimer la ligne indiquée (vers la ligne
237):

    // yield(); // required for ESP8266

Pour en savoir plus, voir ce [forum](https://forums.adafruit.com/viewtopic.php?f=25&t=119136&p=595097&hilit=yield+yield+was+not+declared+#p595097)

# Premiers essais avec un moteur à courant continu

Brancher les deux fils du moteur sur l'un des borniers (M1, ..., M4).
Mettons M1.

Essayer `Exemples -> Adafruit Motor Shield -> DC Motor`.

# Premiers essais avec un moteur pas à pas

Le moteur pas à pas a cinq broches, qu'il faut connecter aux cinq
entrées d'un des deux borniers de la carte d'extension (M1-M2 ou
M3-M4), ce que l'on peut faire avec cinq câbles mâle-mâle. À noter
qu'il faut les mettre dans l'ordre suivant: rose, orange (sur M1/M3),
rouge (sur la masse), jaune, bleu (sur M2/M4).

Pour expérimenter et débrancher et rebrancher régulièrement, ce n'est
pas très pratique. Une autre option est de se bricoler un connecteur
cinq broches, par exemple avec des «stack headers».

TODO: photos + explications

Essayer `Exemples -> Adafruit Motor Shield -> Stepper Test`.

Dans cet exemple, on choisit la vitesse:

    myMotor->setSpeed(10)

puis on indique le nombre de pas à faire et la direction (`FORWARD`:
avant, `BACKWARD`: arrière):

    myMotor->step(100, FORWARD, DOUBLE)

Le dernier argument (`DOUBLE`) gère le type de pas (`DOUBLE`, `SIMPLE`
ou `INTERLEAVED`); chacun a ses avantages; on peut rester sur `DOUBLE`
dans un premier temps.

La commande `step` gère la vitesse en introduisant un délai adéquat
entre chacun des pas; mais du coup elle est bloquante: on ne peut donc
pas exécuter d'autres instructions en même temps; par exemple faire
tourner un deuxième moteur! Pour cela, il peut être pratique
d'utiliser la commande de plus bas niveau:

    myMotor->step(100, FORWARD, DOUBLE)

# Défis

## Un moteur

Faire faire un tour 

## Deux moteurs

Faire tourner deux moteurs pas à pas en même temps, dans le même sens
ou en sens inverse.

## Horloge

Simuler une horloge en équipant le moteur d'une trotteuse en carton,
et lui faire faire un tour par minute. Équiper un deuxième moteur
d'une grande aiguille et lui faire faire un tour en une heure.

TODO: vidéo

## Fusée

Faire décoller et atterrir une fusée depuis une base spatiale (à
l'aide d'un fil nylon discret). Le jeu et de simuler une accélération
très progressive au début, puis de plus en plus rapide.

TODO: vidéo
