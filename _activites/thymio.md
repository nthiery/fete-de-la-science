---
layout: activite
title: Programmation et robots
author: faissole
level: primaire, collège
'2016':
  scolaires:
    salle: Salle 465 - Bâtiment 650 Ada Lovelace
bgcolor: '#66cc99'
keywords: 
logo: logo-thymio.png
images:
- thymio-1.jpg
- thymio-2.jpg
ref: http://www.devoxx4kids.org/materials/workshops/thymio/
---

Cet atelier permet de découvrir des robots Thymio : après une découverte
du robot et de ses capteurs, les élèves devront deviner les
comportements pré-programmés du robot. 

Ils découvriront ensuite les concepts de programme et de programmation
en aidant l'animateur à définir un nouveau comportement.

## Pour en savoir plus

[Le site officiel de Thymio](https://www.thymio.org/fr:thymio)
