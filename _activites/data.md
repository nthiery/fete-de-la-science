---
layout: activite
title: "Comment fabriquer son propre Data Tricorder en 2017?"
author: rafes
level: lycée
'2017':
  scolaires:
    salle: Salle 475 - Bâtiment 650 Ada Lovelace
  grand_public:
    salle: ??? - Bâtiment 450
bgcolor: orange
keywords: 
logo: logo-data.jpg
images: 
myid: data
---

L'humanité depuis une décennie a produit plus d'informations que
durant toute son histoire. Ces informations sont souvent
non-structurés et sont pour beaucoup des informations fausses ou
incomplètes.
Les fondateurs du Web ont proposé des technologies pour permettre au
Web de corriger ses imperfections. En 2017, ces technologies
commencent à être mise en production et sont mises à disposition des
ingénieurs, des chercheurs et des lycéens. L'université Paris-Saclay,
le Center for Data Science et le LRI encouragent ses chercheurs à
partager cette vision en mettant en place des services facilitant le
partage de leurs données afin d'accélérer la recherche.
Durant cet atelier vous apprendrez à interroger simplement les données
déjà accessibles au travers du Web pour découvrir comment
fonctionneront dans un proche avenir les premiers Data Tricorders.
