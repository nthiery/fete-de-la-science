---
layout: activite
title: "La Grande Vitrine de Noël"
author:
- beaudouin-lafon
- belopopsky
- hsueh
- larroche
- mackay
- malloch
level: primaire, collège, lycée
'2016':
  scolaires:
    salle: Atrium - Bâtiment 660 Claude Shannon
bgcolor: '#ffcc99'
keywords: 
logo: logo-vitrine.jpg
images: 
---

Ce projet financé par la [Diagonale
Paris-Saclay](http://www.ladiagonale-paris-saclay.fr/) est une
collaboration entre la troupe de théâtre «n+1», en résidence à l'Agora
d'Evry, et l'équipe ExSitu commune au LRI et à Inria. Il est présenté à
la Fête de la Science en avant-première, sous une forme simplifiée,
avant son installation à l'Agora d'Evry pendant tout le mois de
décembre 2017. 

La Grande Vitrine de Noël est à la fois un automate qui est une machine
à fabriquer des Père Noël, et un calendrier de l'Avent qui révèle
progressivement le fonctionnement de la machine (lorsqu'elle sera
installée à Évry, un panneau sera retiré chaque jour). 

La Grande Vitrine de Noël est également interactive : un Père Noël
réagit aux gestes de la personne en face de l'écran et il s'agit de le
convaincre de faire redémarrer la machine. Mais est-ce le participant
qui contrôle le Père Noël, ou bien l'inverse ?

## Pour aller plus loin

[Présentation complète du
projet](http://www.ladiagonale-paris-saclay.fr/nos-actions/grande-vitrine/)
