---
layout: activite
title: Impression 3D
author:
- bertin
- malaga
level: collège, lycée
'2016':
    grand_public:
      salle: Salle ??? - Bâtiment 336
bgcolor: '#cc99ff'
keywords: 
logo: logo-3d.gif
images:
---

Venez découvrir une imprimante 3D en action, vous initier à la
conception d'objets en trois dimensions, et admirer une collection
d'objets mathématiques imprimés.
