robin, waller

Durée de l'activité : entre 45 minutes et 1h00

Matériel :
- les élastiques avec attaches et pince pouvant coulisser
- ceintures supplémentaires pour ceux qui n'auraient pas de passants à leur pantalon
- fiche-routage, fiche-phrase, fiches-messages à remplir
- de quoi écrire facilement (crayons gras, feutres)
- étiquettes pour identifier les routeurs (papier + pince à linge)

Âge : collège et au-delà, en tout cas maîtrise indispensable de l'écriture et de la lecture

Groupe : essayer d'avoir un groupe homogène et volontaire (activité parfaite pour une demi-classe lors d'une journée de scolaires, risquée avec des personnes prises au
+hasard)

Routeur/ordinateur : de préférence doubler le participant-routeur (qui fait circuler les messages) avec un participant-ordinateur (qui les écrit et y répond).

Taille du réseau : 3 routeurs au moins (alignés puis en triangle). Peut-être commencer par un petit réseau pour tester, puis passer à la plus grande version à 6? Dans ce cas,
+peut-être leur faire découvrir eux-mêmes, avec le petit réseau, qu'il faut indiquer le destinataire et l'expéditeur du message?

Messages : peut-être faire créer un nouveau message pour le message retour, plutôt qu'utiliser le même papier?

Phrase : Expliquer plus clairement la notion de cadavre exquis. Donner un autre titre à la fiche : "la phrase de A" donne l'impression que A doit produire lui-même sa phrase,
+alors qu'il va la reconstituer. Si on demande 3 éléments de phrase, demander "sujet", "verbe", "complément indifférent" (COD, manière, lieu, temps, etc., qui ne posera pas
+le problème du verbe obligatoirement transitif)

Ne pas oublier de leur dire dès le début de l'activité d'observer le fonctionnement du réseau, pour en tirer des conclusions ensuite

Élargissements :
- adresses IP
- plusieurs chemins possibles
- comment gérer la disparition d'une liaison (re-routage, réécriture des tables)
- Communication par paquets pour les gros fichiers
- différence entre Internet et Web
- réseaux publics, réseaux privés
- données en clair, données cryptées
- bonus recherche fondamentale : on a inventé l'idée de paquets avant d'inventer Internet
