---
layout: activite
title: "Le jeu du rond point"
level: primaire, collège, lycée

'2018':
  author:
  - tsanchez
  - robin
  - bartenlian
  - pavao
  scolaires:
    salle: Salle 475 - Bâtiment 650 Ada Lovelace
bgcolor: DeepSkyBlue
keywords: 
logo:
images: 
myid: rond_point
---

Des cases de couleur, des pions qui ne sont pas à la bonne place ...
Comment remettre tout le monde au bon endroit? À travers ce jeu
coopératif, découvrez les notions d'algorithme, de tri circulaire, de
vérification.
