---
layout: activite
title: "La machine de Turing"
author:
- raynaud
level: lycée
'2018':
  grand_public:
    salle: Bâtiment 450
    horaires: 14h, 15h, 16h
bgcolor: LightGray
keywords:
logo: turing.jpg
images:
- machine-turing.jpg
- machine-turing-1.jpg
- machine-turing-2.jpg
myid: machine_turing
---

Première partie: un [film](https://videotheque.cnrs.fr/doc=2975) de
Catherine Bernstein (INRIA-CNRS 2012, 30mn) retraçant la vie d'Alan
Turing, mathématicien de génie.

Deuxième partie: Marc Raynaud nous présentera un 
[prototype](https://machinedeturing.com/)
de la
machine imaginée par Alan Turing en 1936 pour modéliser le concept
d'algorithme. Cette machine est réalisée avec des composants
électriques existants à cette époque. Ses capacités remarquables
rendent hommage au génie de Turing.
