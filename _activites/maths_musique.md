---
layout: activite
title: "Maths et Bach"
author:
- bergeron
level: collège, lycée
'2018':
  grand_public:
    salle: Bâtiment 450
    horaires: 16h
bgcolor: LightYellow # lemonchiffon
keywords:
logo: maths-musique.png
images: maths-musique.png
myid: maths_musique
---

Cette présentation vous invite à voir et entendre certaines des
nombreuses symétries étudiées en mathématiques, mais aussi exploitées
depuis longtemps dans de nombreux domaines des arts et des sciences.
