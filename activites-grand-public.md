---
layout: page
title: Activités pour le grand public 2018
year: '2018'
---

{% for activite in site.activites %}
  {% if activite.url contains "jonglerie" %}
     {% assign jonglerie = activite %}
  {% endif %}
  {% if activite.url contains "musique" %}
     {% assign bach = activite %}
  {% endif %}
{% endfor %}

Voici les activités qui seront présentées au grand public le dimanche
14 octobre 2018 au [bâtiment 450](/pratique#grand_public), de 14h00 à 18h00.

**L'entrée est libre, sans inscription**, en fonction des places
disponibles. Les activités durent environ 30 minutes et sont répétées
plusieurs fois dans l'après-midi.
<!--Nos attractions phares,!-->
Le spectacle
<a href="{{ site.baseurl }}{{ jonglerie.url }}">«{{ site.baseurl }}{{ jonglerie.title }}»</a>
et la conférence
<a href="{{ site.baseurl }}{{ bach.url }}">«{{ site.baseurl }}{{ bach.title }}»</a>
seront présentées respectivement
à {{ jonglerie[page.year].grand_public.horaires }} et 16h.

Télécharger la [plaquette 2018]({{site.baseurl}}/documents/plaquette-public-2018.pdf).

<table>
  <tr>
  {% for activite in site.activites %}
    {% if activite[page.year].grand_public %}
      {% include activite-in-table.html activite=activite year=page.year %}
    {% endif %}
  {% endfor %}
  </tr>
</table>
