---
layout: page
title: Bienvenue sur le site de la Fête de la science au LRI !
---

<!-- TODO: rajouter des liens vers les activités dans les aspects ci-dessous !-->

Comme chaque année, le
[Laboratoire de Recherche en Informatique](http://www.lri.fr) de
l'[Université Paris Sud](http://www.u-psud.fr)
participe<!--a participé!-->
à la [fête de la science](https://www.fetedelascience.fr/)
en proposant une série d'activités pour découvrir
l'informatique sous tous ses aspects, futuristes (écrans géants),
artistiques (jonglerie), matériels (robotique, impression 3D, token),
conceptuels (programmation, modélisation, bioinformatique), citoyens
(logiciels, matériel et données libres), ...

Le vendredi 12 octobre, nous recevrons dans
[six classes]({{site.baseurl}}/activites-scolaires/) du CM1 à la terminale
dans [nos locaux]({{site.baseurl}}/pratique#scolaires),
tandis que le dimanche 14, nous
[accueillerons le grand public]({{site.baseurl}}/activites-grand-public/) au
[bâtiment 450]({{site.baseurl}}/pratique#grand_public).
<!--avons accueilli au bâtiment 450 entre 150 et 200 personnes pour nos huit
[activités grand public]({{site.baseurl}}/activites-grand-public/).!-->


Voici les photos des
<a href="https://sinuaisons.net/2017/10/16/fete-de-la-science-2017//" target="_blank">éditions 2017</a>,
<a href="https://sinuaisons.net/2016/10/16/fete-de-la-science-au-lri-14-et-16-octobre-2016/" target="_blank">2016</a>
et
<a href="https://sinuaisons.net/2015/10/13/fete-de-la-science-lri-orsay-11-octobre-2015/" target="_blank">2015</a>
par Émilia Robin, et des
<a href="https://www.youtube.com/watch?v=PGvtXRYh1Q8&feature=youtu.be" target="_blank">extraits vidéo</a>
de notre attraction phare, le spectacle [«Jonglerie musicale,
Automates et Combinatoire»]({{site.baseurl}}/activites/jonglerie).

<a href="{{site.baseurl}}/public/science_1.jpg"><img class="image"
src="{{site.baseurl}}/public/science_1.jpg" alt="Fête de la Science 2015
au LRI"></a>


